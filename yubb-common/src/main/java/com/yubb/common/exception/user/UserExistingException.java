package com.yubb.common.exception.user;

/**
 *@Description 同浏览器防止不同平台用户登录导致session错乱
 *@Author zhushuyong
 *@Date 2021/6/24 17:27
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public class UserExistingException extends UserException {
    private static final long serialVersionUID = 1L;

    public UserExistingException() {
        super("user.existing", null);
    }
}
