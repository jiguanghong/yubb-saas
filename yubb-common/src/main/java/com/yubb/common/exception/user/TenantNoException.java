package com.yubb.common.exception.user;

/**
 * @Description 租户编号不存在异常类
 * @Author zhushuyong
 * @Date 2021/7/4 22:19
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public class TenantNoException extends UserException {

    private static final long serialVersionUID = 1L;

    public TenantNoException() {
        super("tenant.no.not.exists", null);
    }
}

