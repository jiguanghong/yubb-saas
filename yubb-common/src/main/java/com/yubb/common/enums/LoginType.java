package com.yubb.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Description 不同的登录枚举
 * @Author zhushuyong
 * @Date 2021/7/19 13:33
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum LoginType {

    SAAS_USER("saas_user", "租户用户"),
    PLATFORM_USER("platform_user", "平台用户"),
    ;

    private String type;

    private String typeMsg;

}
