package com.yubb.common.core.domain.platform.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yubb.common.annotation.Excel;
import com.yubb.common.core.domain.platform.base.PlatformVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 租户对象VO
 * 
 * @author zhushuyong
 * @date 2021-07-02
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PlatformTenantVO extends PlatformVO {
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "租户id", cellType = Excel.ColumnType.STRING, prompt = "租户id")
    private String id;

    /** 租户名称 */
    @Excel(name = "租户名称")
    private String tenantName;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenantNo;

    /**
     * 租户logo
     */
    @Excel(name = "租户logo")
    private String logoUrl;

    /** 租户状态（0正常 1停用） */
    @Excel(name = "租户状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 租户系统开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "租户系统开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 租户系统到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "租户系统到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;


    /**
     * 用户id
     */
    private String userId;
    /**
     * 部门id
     */
    private String deptId;
    /** 用户名称 */
    private String userName;

    /** 部门名称 */
    private String deptName;

    /** 登录名称 */
    private String loginName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 用户帐号状态（0正常 1停用） */
    private String userStatus;

}
