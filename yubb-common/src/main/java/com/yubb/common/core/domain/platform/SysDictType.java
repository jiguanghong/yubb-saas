package com.yubb.common.core.domain.platform;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yubb.common.core.domain.platform.base.PlatformDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 *@Description 字典类型表
 *@Author zhushuyong
 *@Date 2021/5/30 23:41
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dict_type")
public class SysDictType extends PlatformDO {
    private static final long serialVersionUID = 1L;

    /** 字典名称 */
    private String dictName;

    /** 字典类型 */
    private String dictType;

    /** 状态（0正常 1停用） */
    private String status;

    /** 备注 */
    private String remark;

}
