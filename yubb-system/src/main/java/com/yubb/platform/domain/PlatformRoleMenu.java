package com.yubb.platform.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色和菜单关联 platform_role_menu
 * 
 * @author ruoyi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("platform_role_menu")
public class PlatformRoleMenu implements Serializable {
    /** 角色ID */
    private String roleId;
    
    /** 菜单ID */
    private String menuId;

}
