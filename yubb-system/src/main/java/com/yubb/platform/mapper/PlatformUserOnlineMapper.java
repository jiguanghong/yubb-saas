package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformUserOnline;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description 平台在线用户
 * @Author zhushuyong
 * @Date 2021/7/30 13:12
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformUserOnlineMapper extends BaseMapper<PlatformUserOnline> {

}
