package com.yubb.platform.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.dto.SysDictTypeDTO;
import com.yubb.common.core.domain.platform.SysDictType;
import com.yubb.common.core.domain.platform.vo.SysDictTypeVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 字典表 数据层
 *@Author zhushuyong
 *@Date 2021/6/17 14:18
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {
    /**
     * 根据条件分页查询字典类型
     * 
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    public List<SysDictTypeVO> selectDictTypeList(SysDictTypeDTO dictType);

}
