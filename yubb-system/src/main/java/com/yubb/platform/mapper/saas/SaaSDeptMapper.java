package com.yubb.platform.mapper.saas;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.saas.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 平台操作租户的部门数据mapper
 *@Author zhushuyong
 *@Date 2021/6/17 14:22
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface SaaSDeptMapper extends BaseMapper<SysDept> {

}
