package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 角色与菜单关联表 数据层
 *@Author zhushuyong
 *@Date 2021/6/14 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
