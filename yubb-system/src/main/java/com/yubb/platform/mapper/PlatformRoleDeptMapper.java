package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformRoleDept;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 角色与部门关联表 数据层
 *@Author zhushuyong
 *@Date 2021/6/19 16:14
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformRoleDeptMapper extends BaseMapper<PlatformRoleDept> {

}
