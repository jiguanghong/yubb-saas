package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformUserPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description 用户岗位mapper
 * @Author zhushuyong
 * @Date 2021/7/30 13:13
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformUserPostMapper extends BaseMapper<PlatformUserPost> {
}
