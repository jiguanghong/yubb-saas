package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformOperLog;
import com.yubb.platform.domain.dto.PlatformOperLogDTO;
import com.yubb.platform.domain.vo.PlatformOperLogVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 *@Description 操作日志 数据层
 *@Author zhushuyong
 *@Date 2021/6/20 20:50
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformOperLogMapper extends BaseMapper<PlatformOperLog> {

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public List<PlatformOperLogVO> selectOperLogList(PlatformOperLogDTO operLog);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();

}
