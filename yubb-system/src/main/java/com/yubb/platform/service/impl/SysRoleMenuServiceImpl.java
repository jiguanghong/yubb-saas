package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.platform.domain.SysRoleMenu;
import com.yubb.platform.mapper.SysRoleMenuMapper;
import com.yubb.platform.service.ISysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * @Description 角色菜单接口实现
 * @Author zhushuyong
 * @Date 2021/6/19 16:01
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
        implements ISysRoleMenuService {
}
