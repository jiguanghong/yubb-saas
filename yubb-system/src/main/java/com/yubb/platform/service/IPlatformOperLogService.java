package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.platform.domain.PlatformOperLog;
import com.yubb.platform.domain.dto.PlatformOperLogDTO;
import com.yubb.platform.domain.vo.PlatformOperLogVO;

import java.util.List;

/**
 *@Description 操作日志 服务层
 *@Author zhushuyong
 *@Date 2021/6/21 22:44
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IPlatformOperLogService extends IService<PlatformOperLog> {

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public List<PlatformOperLogVO> selectOperLogList(PlatformOperLogDTO operLog);

    /**
     * 批量删除系统操作日志
     *
     * @param ids 需要删除的数据
     * @return 结果
     */
    public int deleteOperLogByIds(String ids);

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public PlatformOperLogVO selectOperLogById(String operId);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();

}
