package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.page.MpPageUtils;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.core.text.Convert;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.platform.domain.PlatformNotice;
import com.yubb.platform.domain.dto.PlatformNoticeDTO;
import com.yubb.platform.domain.vo.PlatformNoticeVO;
import com.yubb.platform.mapper.PlatformNoticeMapper;
import com.yubb.platform.service.IPlatformNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

/**
 *@Description 公告 服务层实现
 *@Author zhushuyong
 *@Date 2021/6/21 22:28
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformNoticeServiceImpl extends ServiceImpl<PlatformNoticeMapper, PlatformNotice> implements IPlatformNoticeService {

    @Autowired
    private PlatformNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public PlatformNoticeVO selectNoticeById(String noticeId)
    {
        return DozerUtils.copyProperties(noticeMapper.selectById(noticeId), PlatformNoticeVO.class);
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public TableDataInfo<PlatformNoticeVO> selectNoticeList(PlatformNoticeDTO notice)
    {
        IPage<PlatformNoticeVO> noticeVOIPage = this.page(MpPageUtils.intPage(), new LambdaQueryWrapper<PlatformNotice>()
                .like(StringUtils.isNotBlank(notice.getNoticeTitle()), PlatformNotice::getNoticeTitle, notice.getNoticeTitle())
                .eq(StringUtils.isNotBlank(notice.getNoticeType()), PlatformNotice::getNoticeType, notice.getNoticeType())
                .like(StringUtils.isNotBlank(notice.getCreateBy()), PlatformNotice::getCreateBy, notice.getCreateBy()));
        return MpPageUtils.copyPage(noticeVOIPage, PlatformNoticeVO.class);
    }

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(PlatformNoticeDTO notice)
    {
        return noticeMapper.insert(DozerUtils.copyProperties(notice, PlatformNotice.class));
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(PlatformNoticeDTO notice)
    {
        return noticeMapper.updateById(DozerUtils.copyProperties(notice, PlatformNotice.class));
    }

    /**
     * 删除公告对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(String ids)
    {
        return noticeMapper.deleteBatchIds(Arrays.asList(Convert.toStrArray(ids)));
    }
    
}
