package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.platform.domain.PlatformUserRole;

/**
 * @Description 用户角色接口
 * @Author zhushuyong
 * @Date 2021/6/16 22:25
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IPlatformUserRoleService extends IService<PlatformUserRole> {
}
