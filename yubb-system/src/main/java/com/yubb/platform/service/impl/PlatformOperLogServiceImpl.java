package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.text.Convert;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.platform.domain.PlatformOperLog;
import com.yubb.platform.domain.dto.PlatformOperLogDTO;
import com.yubb.platform.domain.vo.PlatformOperLogVO;
import com.yubb.platform.mapper.PlatformOperLogMapper;
import com.yubb.platform.service.IPlatformOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 *@Description 操作日志 服务层处理
 *@Author zhushuyong
 *@Date 2021/6/21 22:39
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformOperLogServiceImpl extends ServiceImpl<PlatformOperLogMapper, PlatformOperLog> implements IPlatformOperLogService {

    @Autowired
    private PlatformOperLogMapper operLogMapper;

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    @Override
    public List<PlatformOperLogVO> selectOperLogList(PlatformOperLogDTO operLog)
    {
        return operLogMapper.selectOperLogList(operLog);
    }

    /**
     * 批量删除系统操作日志
     *
     * @param ids 需要删除的数据
     * @return
     */
    @Override
    public int deleteOperLogByIds(String ids)
    {
        return operLogMapper.deleteBatchIds(Arrays.asList(Convert.toStrArray(ids)));
    }

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    @Override
    public PlatformOperLogVO selectOperLogById(String operId)
    {
        return DozerUtils.copyProperties(operLogMapper.selectById(operId), PlatformOperLogVO.class);
    }

    /**
     * 清空操作日志
     */
    @Override
    public void cleanOperLog()
    {
        operLogMapper.cleanOperLog();
    }
    
}
