package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.platform.domain.PlatformRoleMenu;

/**
 * @Description 平台角色菜单接口
 * @Author zhushuyong
 * @Date 2021/7/15 22:25
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IPlatformRoleMenuService extends IService<PlatformRoleMenu> {
}
