package com.yubb.platform.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.constant.ShiroConstants;
import com.yubb.common.utils.DateUtils;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.platform.domain.PlatformUserOnline;
import com.yubb.platform.domain.vo.PlatformUserOnlineVO;
import com.yubb.platform.mapper.PlatformUserOnlineMapper;
import com.yubb.platform.service.IPlatformUserOnlineService;
import com.yubb.system.domain.SysUserOnline;
import com.yubb.system.domain.vo.SysUserOnlineVO;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.List;

/**
 *@Description 在线用户 服务层处理
 *@Author zhushuyong
 *@Date 2021/6/21 22:51
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformUserOnlineServiceImpl extends ServiceImpl<PlatformUserOnlineMapper, PlatformUserOnline>
        implements IPlatformUserOnlineService {

    @Autowired
    private IPlatformUserOnlineService userOnlineDao;

    @Autowired
    private PlatformUserOnlineMapper userOnlineMapper;
    
    @Autowired
    private EhCacheManager ehCacheManager;

    /**
     * 通过会话序号查询信息
     *
     * @param sessionId 会话ID
     * @return 在线用户信息
     */
    @Override
    public PlatformUserOnline selectOnlineById(String sessionId)
    {
        return userOnlineDao.getById(sessionId);
    }

    /**
     * 通过会话序号删除信息
     *
     * @param sessionId 会话ID
     * @return 在线用户信息
     */
    @Override
    public void deleteOnlineById(String sessionId)
    {
        userOnlineDao.removeById(sessionId);
    }

    /**
     * 通过会话序号删除信息
     *
     * @param sessions 会话ID集合
     * @return 在线用户信息
     */
    @Override
    public void batchDeleteOnline(List<String> sessions)
    {
        userOnlineDao.removeByIds(sessions);
    }

    /**
     * 保存会话信息
     *
     * @param online 会话信息
     */
    @Override
    public void saveOnline(PlatformUserOnline online)
    {
        userOnlineDao.saveOrUpdate(online);
    }

    /**
     * 查询会话集合
     *
     * @param userOnline 在线用户
     */
    @Override
    public List<PlatformUserOnline> selectUserOnlineList(PlatformUserOnline userOnline)
    {
        return userOnlineDao.list(
                        new LambdaQueryWrapper<PlatformUserOnline>()
                                .like(StringUtils.isNotBlank(userOnline.getIpaddr()), PlatformUserOnline::getIpaddr, userOnline.getIpaddr())
                                .like(StringUtils.isNotBlank(userOnline.getLoginName()), PlatformUserOnline::getLoginName, userOnline.getLoginName()));
    }

    /**
     * 强退用户
     *
     * @param sessionId 会话ID
     */
    @Override
    public void forceLogout(String sessionId)
    {
        userOnlineDao.removeById(sessionId);
    }
    
    /**
     * 清理用户缓存
     *
     * @param loginName 登录名称
     * @param sessionId 会话ID
     */
    @Override
    public void removeUserCache(String loginName, String sessionId)
    {
        Cache<String, Deque<Serializable>> cache = ehCacheManager.getCache(ShiroConstants.PLATFORM_USERCACHE);
        Deque<Serializable> deque = cache.get(loginName);
        if (StringUtils.isEmpty(deque) || deque.size() == 0)
        {
            return;
        }
        deque.remove(sessionId);
    }

    /**
     * 查询会话集合
     *
     * @param expiredDate 失效日期
     */
    @Override
    public List<PlatformUserOnlineVO> selectOnlineByExpired(Date expiredDate)
    {
        List<PlatformUserOnline> list = userOnlineMapper.selectList(new LambdaQueryWrapper<PlatformUserOnline>()
                .le(PlatformUserOnline::getLastAccessTime, DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, expiredDate))
                .orderByAsc(PlatformUserOnline::getLastAccessTime));
        if (!CollUtil.isEmpty(list)) {
            return BeanUtil.copyToList(list, PlatformUserOnlineVO.class);
        }
        return new ArrayList<>();
    }

}
