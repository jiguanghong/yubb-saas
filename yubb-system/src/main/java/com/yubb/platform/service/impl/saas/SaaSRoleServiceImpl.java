package com.yubb.platform.service.impl.saas;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.constant.Constants;
import com.yubb.common.core.domain.platform.PlatformUser;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.entity.SysRole;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;
import com.yubb.common.core.text.Convert;
import com.yubb.common.exception.BusinessException;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.platform.domain.SysRoleMenu;
import com.yubb.platform.mapper.saas.SaaSRoleMapper;
import com.yubb.platform.mapper.saas.SaaSUserRoleMapper;
import com.yubb.platform.service.ISysRoleMenuService;
import com.yubb.platform.service.saas.ISaaSRoleService;
import com.yubb.system.domain.SysUserRole;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *@Description 角色 业务层处理
 *@Author zhushuyong
 *@Date 2021/6/6 16:46
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SaaSRoleServiceImpl extends ServiceImpl<SaaSRoleMapper, SysRole> implements ISaaSRoleService {

    @Resource
    private SaaSRoleMapper saaSRoleMapper;

    @Resource
    private ISysRoleMenuService roleMenuService;

    @Resource
    private SaaSUserRoleMapper saaSUserRoleMapper;

    /**
     * 根据条件分页查询角色数据
     * 
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    @Override
    public List<SysRoleVO> selectRoleList(SysRoleDTO roleDTO) {
        return saaSRoleMapper.selectRoleList(roleDTO);
    }

    @Override
    public List<SysRoleVO> selectPfRoleList(SysRoleDTO roleDTO) {
        return saaSRoleMapper.selectPfRoleList(roleDTO);
    }

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    public SysRoleVO selectRoleById(String roleId) {
        return DozerUtils.copyProperties(saaSRoleMapper.selectOne(Wrappers.query(SysRole.builder()
                .id(roleId)
                .delFlag(Constants.DEL_FLAG_NORMAL)
                .build())),SysRoleVO.class);
    }

    /**
     * 批量删除角色信息
     * 
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    @Transactional
    public int deleteRoleByIds(String ids, PlatformUser platformUser) {
        String[] roleIds = Convert.toStrArray(ids);
        for (String roleId : roleIds) {
            checkRoleAllowed(SysRoleDTO.builder().id(roleId).build(), platformUser);
            SysRoleVO role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new BusinessException(String.format("%1$s已被租户用户分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        roleMenuService.remove(new LambdaQueryWrapper<SysRoleMenu>().in(SysRoleMenu::getRoleId, Arrays.asList(roleIds)));
        return saaSRoleMapper.deleteBatchIds(Arrays.asList(roleIds));
    }

    /**
     * 新增保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean insertRole(SysRoleDTO roleDTO) {
        // 新增角色信息
        SysRole sysRole = DozerUtils.copyProperties(roleDTO, SysRole.class);
        sysRole.setTenantId(Constants.PLATFORM_SAAS);
        sysRole.setRoleKey(IdUtil.fastSimpleUUID());
        saaSRoleMapper.insert(sysRole);
        roleDTO.setId(sysRole.getId());
        return insertRoleMenu(roleDTO);
    }

    /**
     * 修改保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean updateRole(SysRoleDTO roleDTO) {
        // 修改角色信息
        saaSRoleMapper.updateById(DozerUtils.copyProperties(roleDTO, SysRole.class));
        // 删除角色与菜单关联
        roleMenuService.remove(Wrappers.query(SysRoleMenu.builder().roleId(roleDTO.getId()).build()));
        return insertRoleMenu(roleDTO);
    }

    /**
     * 新增角色菜单信息
     * 
     * @param role 角色对象
     */
    public boolean insertRoleMenu(SysRoleDTO role) {
        // 新增角色与菜单管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        boolean rows = true;
        for (String menuId : role.getMenuIds()) {
            list.add(SysRoleMenu.builder().roleId(role.getId()).menuId(menuId).build());
        }
        if (list.size() > 0) {
            rows = roleMenuService.saveBatch(list);
        }
        return rows;
    }

    /**
     * 校验角色是否允许操作
     * 
     * @param roleDTO 角色信息
     */
    @Override
    public void checkRoleAllowed(SysRoleDTO roleDTO, PlatformUser platformUser) {
        if (!platformUser.isAdmin() && StringUtils.isNotBlank(roleDTO.getId()) && DozerUtils.copyProperties(roleDTO, SysRole.class).isAdmin()) {
            throw new BusinessException("只有平台超管才能修改租户超管角色");
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public long countUserRoleByRoleId(String roleId) {
        return saaSUserRoleMapper.selectCount(Wrappers.query(SysUserRole.builder().roleId(roleId).build()));
    }

    /**
     * 角色状态修改
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    public int changeStatus(SysRoleDTO roleDTO) {
        return saaSRoleMapper.updateById(DozerUtils.copyProperties(roleDTO,SysRole.class));
    }

}
