package com.yubb.platform.service.saas;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.platform.PlatformUser;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.entity.SysRole;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;

import java.util.List;

/**
 *@Description 角色业务层
 *@Author zhushuyong
 *@Date 2021/6/6 16:51
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISaaSRoleService extends IService<SysRole> {

    /**
     * 根据条件分页查询角色数据
     * 
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleVO> selectRoleList(SysRoleDTO roleDTO);

    /**
     * 租户根据条件分页查询角色数据
     *
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleVO> selectPfRoleList(SysRoleDTO roleDTO);

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public SysRoleVO selectRoleById(String roleId);

    /**
     * 批量删除角色用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deleteRoleByIds(String ids, PlatformUser platformUser);

    /**
     * 新增保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    public boolean insertRole(SysRoleDTO roleDTO);

    /**
     * 修改保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    public boolean updateRole(SysRoleDTO roleDTO);

    /**
     * 校验角色是否允许操作
     * 
     * @param roleDTO 角色信息
     */
    public void checkRoleAllowed(SysRoleDTO roleDTO, PlatformUser platformUser);

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public long countUserRoleByRoleId(String roleId);

    /**
     * 角色状态修改
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int changeStatus(SysRoleDTO role);

}
