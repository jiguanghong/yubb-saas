package com.yubb.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *@Description 角色和部门关联
 *@Author zhushuyong
 *@Date 2021/5/31 22:52
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("sys_role_dept")
public class SysRoleDept implements Serializable {
    /** 角色ID */
    private String roleId;
    
    /** 部门ID */
    private String deptId;

    /**
     * 租户id
     */
    private String tenantId;

}
