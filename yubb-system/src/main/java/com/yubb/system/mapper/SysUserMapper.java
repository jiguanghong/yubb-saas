package com.yubb.system.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.constant.ShiroConstants;
import com.yubb.common.core.domain.saas.dto.SysUserDTO;
import com.yubb.common.core.domain.saas.entity.SysUser;
import com.yubb.common.core.domain.saas.vo.SysUserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *@Description 用户表 数据层
 *@Author zhushuyong
 *@Date 2021/6/1 9:36
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 根据条件分页查询用户列表
     * 
     * @param sysUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUserVO> selectUserList(SysUserDTO sysUserDTO);

    /**
     * 根据条件分页查询未已配用户角色列表
     * 
     * @param sysUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUserVO> selectAllocatedList(SysUserDTO sysUserDTO);

    /**
     * 根据条件分页查询未分配用户角色列表
     * 
     * @param sysUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUserVO> selectUnallocatedList(SysUserDTO sysUserDTO);

    /**
     * 通过用户ID查询用户
     * 
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUserVO selectUserById(String userId);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @InterceptorIgnore(tenantLine = "true")
    public SysUserVO selectUserByLoginNameAndTenantId(@Param("userName") String userName,
                                                      @Param(ShiroConstants.TENANT_ID) String tenantId);

}
