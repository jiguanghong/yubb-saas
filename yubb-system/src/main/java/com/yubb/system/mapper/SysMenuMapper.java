package com.yubb.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.SysMenu;
import com.yubb.common.core.domain.platform.vo.SysMenuVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description TODO
 * @Author zhushuyong
 * @Date 2021/8/3 16:41
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 查询系统所有菜单（含按钮）
     *
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuAll();

    /**
     * 根据角色ID查询菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    public List<String> selectMenuTree(String roleId);

}
