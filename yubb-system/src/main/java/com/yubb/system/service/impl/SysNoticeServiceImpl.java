package com.yubb.system.service.impl;

import java.util.Arrays;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.page.MpPageUtils;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.system.domain.dto.SysNoticeDTO;
import com.yubb.system.domain.vo.SysNoticeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yubb.common.core.text.Convert;
import com.yubb.system.domain.SysNotice;
import com.yubb.system.mapper.SysNoticeMapper;
import com.yubb.system.service.ISysNoticeService;

/**
 *@Description 公告 服务层实现
 *@Author zhushuyong
 *@Date 2021/6/21 22:28
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper ,SysNotice> implements ISysNoticeService {

    @Autowired
    private SysNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     * 
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNoticeVO selectNoticeById(String noticeId)
    {
        return DozerUtils.copyProperties(noticeMapper.selectById(noticeId), SysNoticeVO.class);
    }

    /**
     * 查询公告列表
     * 
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public TableDataInfo<SysNoticeVO> selectNoticeList(SysNoticeDTO notice)
    {
        return MpPageUtils.copyPage(this.page(MpPageUtils.intPage(), new LambdaQueryWrapper<SysNotice>()
                        .like(StringUtils.isNotBlank(notice.getNoticeTitle()), SysNotice::getNoticeTitle, notice.getNoticeTitle())
                        .eq(StringUtils.isNotBlank(notice.getNoticeType()), SysNotice::getNoticeType, notice.getNoticeType())
                        .like(StringUtils.isNotBlank(notice.getCreateBy()), SysNotice::getCreateBy, notice.getCreateBy())), SysNoticeVO.class);
    }

    /**
     * 新增公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(SysNoticeDTO notice)
    {
        return noticeMapper.insert(DozerUtils.copyProperties(notice, SysNotice.class));
    }

    /**
     * 修改公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(SysNoticeDTO notice)
    {
        return noticeMapper.updateById(DozerUtils.copyProperties(notice, SysNotice.class));
    }

    /**
     * 删除公告对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(String ids)
    {
        return noticeMapper.deleteBatchIds(Arrays.asList(Convert.toStrArray(ids)));
    }
}
