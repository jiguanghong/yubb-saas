package com.yubb.system.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.domain.saas.dto.SysDeptDTO;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.vo.SysDeptVO;
import com.yubb.common.utils.bean.DozerUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yubb.common.annotation.DataScope;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.saas.entity.SysDept;
import com.yubb.common.exception.BusinessException;
import com.yubb.common.utils.StringUtils;
import com.yubb.system.mapper.SysDeptMapper;
import com.yubb.system.service.ISysDeptService;
import static com.yubb.common.constant.Constants.DEL_FLAG_DEL;
import static com.yubb.common.constant.Constants.DEL_FLAG_NORMAL;

/**
 *@Description 部门管理 服务实现
 *@Author zhushuyong
 *@Date 2021/6/17 14:23
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept>
        implements ISysDeptService {

    @Autowired
    private SysDeptMapper deptMapper;

    /**
     * 查询部门管理数据
     * 
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysDeptVO> selectDeptList(SysDeptDTO dept) {
        return deptMapper.selectDeptList(dept);
    }

    /**
     * 查询部门管理树
     * 
     * @param dept 部门信息
     * @return 所有部门信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<Ztree> selectDeptTree(SysDeptDTO dept) {
        List<SysDeptVO> deptList = deptMapper.selectDeptList(dept);
        List<Ztree> ztrees = initZtree(deptList);
        return ztrees;
    }

    /**
     * 查询部门管理树（排除下级）
     * 
     * @param dept 部门信息
     * @return 所有部门信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<Ztree> selectDeptTreeExcludeChild(SysDeptDTO dept)
    {
        String deptId = dept.getId();
        List<SysDeptVO> deptList = deptMapper.selectDeptList(dept);
        Iterator<SysDeptVO> it = deptList.iterator();
        while (it.hasNext())
        {
            SysDeptVO d = (SysDeptVO) it.next();
            if (d.getId().equals(deptId)
                    || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + ""))
            {
                it.remove();
            }
        }
        List<Ztree> ztrees = initZtree(deptList);
        return ztrees;
    }

    /**
     * 根据角色ID查询部门（数据权限）
     *
     * @param role 角色对象
     * @return 部门列表（数据权限）
     */
    @Override
    public List<Ztree> roleDeptTreeData(SysRoleDTO role)
    {
        String roleId = role.getId();
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<SysDeptVO> deptList = selectDeptList(new SysDeptDTO());
        if (StringUtils.isNotNull(roleId))
        {
            List<String> roleDeptList = deptMapper.selectRoleDeptTree(roleId);
            ztrees = initZtree(deptList, roleDeptList);
        }
        else
        {
            ztrees = initZtree(deptList);
        }
        return ztrees;
    }

    /**
     * 对象转部门树
     *
     * @param deptList 部门列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<SysDeptVO> deptList)
    {
        return initZtree(deptList, null);
    }

    /**
     * 对象转部门树
     *
     * @param deptList 部门列表
     * @param roleDeptList 角色已存在菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<SysDeptVO> deptList, List<String> roleDeptList)
    {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleDeptList);
        for (SysDeptVO dept : deptList)
        {
            if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()))
            {
                Ztree ztree = new Ztree();
                ztree.setId(dept.getId());
                ztree.setpId(dept.getParentId());
                ztree.setName(dept.getDeptName());
                ztree.setTitle(dept.getDeptName());
                if (isCheck)
                {
                    ztree.setChecked(roleDeptList.contains(dept.getId() + dept.getDeptName()));
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }

    /**
     * 查询部门人数
     * 
     * @param parentId 部门ID
     * @return 结果
     */
    @Override
    public long selectDeptCount(String parentId)
    {
        SysDeptDTO dept = SysDeptDTO.builder().parentId(parentId).build();
        return deptMapper.selectCount(new LambdaQueryWrapper<SysDept>()
                .eq(SysDept::getDelFlag, DEL_FLAG_NORMAL)
                .eq(StringUtils.isNotBlank(dept.getId()), SysDept::getId, dept.getId())
                .eq(StringUtils.isNotBlank(dept.getParentId()), SysDept::getParentId, dept.getParentId()));
    }

    /**
     * 查询部门是否存在用户
     * 
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(String deptId)
    {
        long result = deptMapper.selectCount(Wrappers.query(SysDept.builder()
                .id(deptId)
                .delFlag(DEL_FLAG_NORMAL).build()));
        return result > 0 ? true : false;
    }

    /**
     * 删除部门管理信息
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public int deleteDeptById(String deptId)
    {
        return deptMapper.update(SysDept.builder().delFlag(DEL_FLAG_DEL).build(),
                Wrappers.query(SysDept.builder().id(deptId).build()));
    }

    /**
     * 新增保存部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int insertDept(SysDeptDTO dept)
    {
        SysDeptVO info = deptMapper.selectDeptById(dept.getParentId());
        // 如果父节点不为"正常"状态,则不允许新增子节点
        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus()))
        {
            throw new BusinessException("部门停用，不允许新增");
        }
        dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
        return deptMapper.insert(DozerUtils.copyProperties(dept, SysDept.class));
    }

    /**
     * 修改保存部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateDept(SysDeptDTO dept)
    {
        SysDeptVO newParentDept = deptMapper.selectDeptById(dept.getParentId());
        SysDeptVO oldDept = selectDeptById(dept.getId());
        if (StringUtils.isNotNull(newParentDept) && StringUtils.isNotNull(oldDept))
        {
            String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getId();
            String oldAncestors = oldDept.getAncestors();
            dept.setAncestors(newAncestors);
            updateDeptChildren(dept.getId(), newAncestors, oldAncestors);
        }
        int result = deptMapper.updateById(DozerUtils.copyProperties(dept, SysDept.class));
        if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()))
        {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatus(dept);
        }
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     * 
     * @param dept 当前部门
     */
    private void updateParentDeptStatus(SysDeptDTO dept)
    {
        String updateBy = dept.getUpdateBy();
        dept = DozerUtils.copyProperties(deptMapper.selectDeptById(dept.getId()), SysDeptDTO.class);
        dept.setUpdateBy(updateBy);
        deptMapper.updateDeptStatus(dept);
    }

    /**
     * 修改子元素关系
     * 
     * @param deptId 被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(String deptId, String newAncestors, String oldAncestors)
    {
        List<SysDeptVO> children = deptMapper.selectChildrenDeptById(deptId);
        for (SysDeptVO child : children)
        {
            child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
        }
        if (children.size() > 0)
        {
            deptMapper.updateDeptChildren(BeanUtil.copyToList(children, SysDeptDTO.class));
        }
    }

    /**
     * 根据部门ID查询信息
     * 
     * @param deptId 部门ID
     * @return 部门信息
     */
    @Override
    public SysDeptVO selectDeptById(String deptId)
    {
        return deptMapper.selectDeptById(deptId);
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     * 
     * @param deptId 部门ID
     * @return 子部门数
     */
    @Override
    public int selectNormalChildrenDeptById(String deptId)
    {
        return deptMapper.selectNormalChildrenDeptById(deptId);
    }

    /**
     * 校验部门名称是否唯一
     * 
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(SysDeptDTO dept)
    {
        String deptId = StringUtils.isIdNull(dept.getId());
        Optional<SysDept> info = deptMapper.selectList(Wrappers.query(SysDept.builder()
                .deptName(dept.getDeptName())
                .parentId(dept.getParentId()).build())).stream().findFirst();
        if (info.isPresent() && !deptId.equals(info.get().getId()))
        {
            return UserConstants.DEPT_NAME_NOT_UNIQUE;
        }
        return UserConstants.DEPT_NAME_UNIQUE;
    }

}
