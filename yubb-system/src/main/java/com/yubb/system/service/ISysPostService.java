package com.yubb.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.system.domain.SysPost;
import com.yubb.system.domain.dto.SysPostDTO;
import com.yubb.system.domain.vo.SysPostVO;

/**
 *@Description 岗位信息 服务层
 *@Author zhushuyong
 *@Date 2021/5/27 21:31
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysPostService extends IService<SysPost> {
    /**
     * 查询岗位信息集合
     * 
     * @param postDTO 岗位信息
     * @return 岗位信息集合
     */
    public TableDataInfo<SysPostVO> selectPostPage(SysPostDTO postDTO);

    /**
     * 查询岗位信息集合
     *
     * @param postDTO 岗位信息
     * @return 岗位信息集合
     */
    public List<SysPostVO> selectPostList(SysPostDTO postDTO);

    /**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
    public List<SysPostVO> selectPostAll();

    /**
     * 根据用户ID查询岗位
     * 
     * @param userId 用户ID
     * @return 岗位列表
     */
    public List<SysPostVO> selectPostsByUserId(String userId);

    /**
     * 通过岗位ID查询岗位信息
     * 
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    public SysPostVO selectPostById(String postId);

    /**
     * 批量删除岗位信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deletePostByIds(String ids) throws Exception;

    /**
     * 新增保存岗位信息
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    public int insertPost(SysPostDTO postDTO);

    /**
     * 修改保存岗位信息
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    public int updatePost(SysPostDTO postDTO);

    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    public long countUserPostById(String postId);

    /**
     * 校验岗位名称
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    public String checkPostNameUnique(SysPostDTO postDTO);

    /**
     * 校验岗位编码
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    public String checkPostCodeUnique(SysPostDTO postDTO);
}
