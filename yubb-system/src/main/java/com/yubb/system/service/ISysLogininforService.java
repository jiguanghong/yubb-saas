package com.yubb.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.system.domain.SysLogininfor;
import com.yubb.system.domain.dto.SysLogininforDTO;
import com.yubb.system.domain.vo.SysLogininforVO;

/**
 *@Description 系统访问日志情况信息 服务层
 *@Author zhushuyong
 *@Date 2021/6/21 17:30
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysLogininforService extends IService<SysLogininfor> {
    /**
     * 新增系统登录日志
     * 
     * @param logininfor 访问日志对象
     */
    public void insertLogininfor(SysLogininforDTO logininfor);

    /**
     * 查询系统登录日志集合
     * 
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    public List<SysLogininforVO> selectLogininforList(SysLogininforDTO logininfor);

    /**
     * 批量删除系统登录日志
     * 
     * @param ids 需要删除的数据
     * @return
     */
    public int deleteLogininforByIds(String ids);

    /**
     * 清空系统登录日志
     */
    public void cleanLogininfor();
}
