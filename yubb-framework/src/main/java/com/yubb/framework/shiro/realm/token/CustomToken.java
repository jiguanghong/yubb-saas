package com.yubb.framework.shiro.realm.token;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @Description TODO
 * @Author zhushuyong
 * @Date 2021/7/18 20:37
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public class CustomToken extends UsernamePasswordToken {

    //定义登陆的类型是为了在后面的校验中 去选择使用哪一个realm
    private String loginType;

    /**
     * 租户编号
     */
    private String tenantNo;

    public CustomToken(String userName,String password,boolean rememberMe,String loginType){
        super(userName,password,rememberMe);
        this.loginType=loginType;
    }

    public CustomToken(String userName,String password,boolean rememberMe,String tenantNo,String loginType){
        super(userName,password,rememberMe);
        this.tenantNo = tenantNo;
        this.loginType=loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLoginType() {
        return loginType;
    }

    public String getTenantNo() {
        return tenantNo;
    }

    public void setTenantNo(String tenantNo) {
        this.tenantNo = tenantNo;
    }
}
