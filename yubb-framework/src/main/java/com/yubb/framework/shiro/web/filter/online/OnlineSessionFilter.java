package com.yubb.framework.shiro.web.filter.online;

import com.yubb.common.constant.ShiroConstants;
import com.yubb.common.core.domain.platform.PlatformDept;
import com.yubb.common.core.domain.platform.vo.PlatformUserVO;
import com.yubb.common.core.domain.saas.vo.SysUserVO;
import com.yubb.common.enums.LoginType;
import com.yubb.common.enums.OnlineStatus;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.spring.SpringUtils;
import com.yubb.framework.shiro.session.OnlineSession;
import com.yubb.framework.shiro.session.OnlineSessionDAO;
import com.yubb.platform.service.IPlatformDeptService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 *@Description 自定义访问控制
 *@Author zhushuyong
 *@Date 2021/6/24 17:30
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public class OnlineSessionFilter extends AccessControlFilter
{
    /**
     * 强制退出后重定向的地址
     */
    @Value("${shiro.user.loginUrl}")
    private String loginUrl;

    private OnlineSessionDAO onlineSessionDAO;

    /**
     * 表示是否允许访问；mappedValue就是[urls]配置中拦截器参数部分，如果允许访问返回true，否则false；
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
            throws Exception
    {
        Subject subject = getSubject(request, response);
        if (subject == null || subject.getSession() == null)
        {
            return true;
        }
        Session session = onlineSessionDAO.readSession(subject.getSession().getId());
        if (session != null && session instanceof OnlineSession)
        {
            Object obj = SecurityUtils.getSubject().getPrincipal();
            OnlineSession onlineSession = (OnlineSession) session;
            if (obj instanceof SysUserVO){
                onlineSession.setType(LoginType.SAAS_USER.getType());
            } else if (obj instanceof PlatformUserVO) {
                onlineSession.setType(LoginType.PLATFORM_USER.getType());
            }
            request.setAttribute(ShiroConstants.ONLINE_SESSION, onlineSession);
            // 把user对象设置进去
            boolean isGuest = onlineSession.getUserId() == null || "0".equals(onlineSession.getUserId());
            if (isGuest == true)
            {
                if (obj instanceof SysUserVO){
                    SysUserVO user = ShiroUtils.getSysUser();
                    if (user != null)
                    {
                        onlineSession.setUserId(user.getId());
                        onlineSession.setLoginName(user.getLoginName());
                        onlineSession.setAvatar(user.getAvatar());
                        onlineSession.setDeptName(user.getDept().getDeptName());
                        onlineSession.markAttributeChanged();
                        onlineSession.setTenantId(user.getTenantId());
                    }
                } else if (obj instanceof PlatformUserVO) {
                    PlatformUserVO user = ShiroUtils.getPlatformUser();
                    if (user != null)
                    {
                        onlineSession.setUserId(user.getId());
                        onlineSession.setLoginName(user.getLoginName());
                        onlineSession.setAvatar(user.getAvatar());
                        if (StringUtils.isNotEmpty(user.getDeptId())) {
                            PlatformDept platformDept = SpringUtils.getBean(IPlatformDeptService.class).getById(user.getDeptId());
                            if (!Objects.isNull(platformDept)) {
                                onlineSession.setDeptName(platformDept.getDeptName());
                            }
                        }
                        onlineSession.markAttributeChanged();
                    }
                }

            }

            if (onlineSession.getStatus() == OnlineStatus.off_line)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 表示当访问拒绝时是否已经处理了；如果返回true表示需要继续处理；如果返回false表示该拦截器实例已经处理了，将直接返回即可。
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception
    {
        Subject subject = getSubject(request, response);
        if (subject != null)
        {
            subject.logout();
        }
        saveRequestAndRedirectToLogin(request, response);
        return false;
    }

    // 跳转到登录页
    @Override
    protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException
    {
        WebUtils.issueRedirect(request, response, loginUrl);
    }

    public void setOnlineSessionDAO(OnlineSessionDAO onlineSessionDAO)
    {
        this.onlineSessionDAO = onlineSessionDAO;
    }
}
