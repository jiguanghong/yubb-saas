package com.yubb.framework.shiro.service;

import com.yubb.common.enums.LoginType;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.framework.shiro.session.OnlineSession;
import com.yubb.framework.shiro.session.UserOnline;
import com.yubb.platform.domain.PlatformUserOnline;
import com.yubb.platform.service.IPlatformUserOnlineService;
import com.yubb.system.domain.vo.SysUserOnlineVO;
import com.yubb.system.service.ISysUserOnlineService;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;

/**
 *@Description 会话db操作处理
 *@Author zhushuyong
 *@Date 2021/6/24 17:19
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Component
public class SysShiroService
{
    @Autowired
    private ISysUserOnlineService onlineService;

    @Autowired
    private IPlatformUserOnlineService platformUserOnlineService;

    /**
     * 删除会话
     *
     * @param onlineSession 会话信息
     */
    public void deleteSession(OnlineSession onlineSession)
    {
        onlineService.deleteOnlineById(String.valueOf(onlineSession.getId()));
        platformUserOnlineService.removeById(onlineSession.getId());
    }

    /**
     * 获取会话信息
     *
     * @param sessionId
     * @return
     */
    public Session getSession(Serializable sessionId)
    {
        SysUserOnlineVO userOnline = onlineService.selectOnlineById(String.valueOf(sessionId));
        PlatformUserOnline platformUserOnline = platformUserOnlineService.getById(sessionId);
        if (!Objects.isNull(userOnline)){
            UserOnline online = DozerUtils.copyProperties(userOnline, UserOnline.class);
            online.setType(LoginType.SAAS_USER.getType());
            return createSession(online);
        }
        if (!Objects.isNull(platformUserOnline)){
            UserOnline online = DozerUtils.copyProperties(platformUserOnline, UserOnline.class);
            online.setType(LoginType.PLATFORM_USER.getType());
            return createSession(online);
        }
        return null;
    }

    public Session createSession(UserOnline userOnline)
    {

        OnlineSession onlineSession = new OnlineSession();
        if (StringUtils.isNotNull(userOnline))
        {
            onlineSession.setId(userOnline.getId());
            onlineSession.setHost(userOnline.getIpaddr());
            onlineSession.setBrowser(userOnline.getBrowser());
            onlineSession.setOs(userOnline.getOs());
            onlineSession.setDeptName(userOnline.getDeptName());
            onlineSession.setLoginName(userOnline.getLoginName());
            onlineSession.setStartTimestamp(userOnline.getStartTimestamp());
            onlineSession.setLastAccessTime(userOnline.getLastAccessTime());
            onlineSession.setTimeout(userOnline.getExpireTime());
            onlineSession.setType(userOnline.getType());
        }
        return onlineSession;
    }
}
