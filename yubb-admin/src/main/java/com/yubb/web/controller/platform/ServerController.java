package com.yubb.web.controller.platform;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.yubb.common.core.controller.BaseController;
import com.yubb.framework.web.domain.Server;

/**
 *@Description 服务器监控
 *@Author zhushuyong
 *@Date 2021/5/26 14:41
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/monitor/server")
public class ServerController extends BaseController
{
    private String prefix = "platform/monitor/server";

    @RequiresPermissions("platform:monitor:server:view")
    @GetMapping()
    public String server(ModelMap mmap) throws Exception
    {
        Server server = new Server();
        server.copyTo();
        mmap.put("server", server);
        return prefix + "/server";
    }
}
