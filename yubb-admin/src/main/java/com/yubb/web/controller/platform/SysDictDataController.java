package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.platform.dto.SysDictDataDTO;
import com.yubb.common.core.domain.platform.vo.SysDictDataVO;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.platform.service.ISysDictDataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 数据字典信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:55
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/dict/data")
public class SysDictDataController extends BaseController
{
    private String prefix = "platform/dict/data";

    @Autowired
    private ISysDictDataService dictDataService;

    @RequiresPermissions("platform:dict:view")
    @GetMapping()
    public String dictData()
    {
        return prefix + "/data";
    }

    @PostMapping("/list")
    @RequiresPermissions("platform:dict:list")
    @ResponseBody
    public TableDataInfo list(SysDictDataDTO dictData)
    {
        startPage();
        List<SysDictDataVO> list = dictDataService.selectDictDataList(dictData);
        return getDataTable(list);
    }

    @PlatformLog(title = "字典数据", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:dict:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysDictDataDTO dictData)
    {
        List<SysDictDataVO> list = dictDataService.selectDictDataList(dictData);
        ExcelUtil<SysDictDataVO> util = new ExcelUtil<SysDictDataVO>(SysDictDataVO.class);
        return util.exportExcel(list, "字典数据");
    }

    /**
     * 新增字典类型
     */
    @GetMapping("/add/{dictType}")
    public String add(@PathVariable("dictType") String dictType, ModelMap mmap)
    {
        mmap.put("dictType", dictType);
        return prefix + "/add";
    }

    /**
     * 新增保存字典类型
     */
    @PlatformLog(title = "字典数据", businessType = BusinessType.INSERT)
    @RequiresPermissions("platform:dict:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysDictDataDTO dict)
    {
        dict.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(dictDataService.insertDictData(dict));
    }

    /**
     * 修改字典类型
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("dict", dictDataService.selectDictDataById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存字典类型
     */
    @PlatformLog(title = "字典数据", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:dict:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysDictDataDTO dict)
    {
        dict.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(dictDataService.updateDictData(dict));
    }

    @PlatformLog(title = "字典数据", businessType = BusinessType.DELETE)
    @RequiresPermissions("platform:dict:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(dictDataService.deleteDictDataByIds(ids));
    }
}
