package com.yubb.web.controller.platform.saas;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.constant.Constants;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.platform.dto.SysMenuDTO;
import com.yubb.common.core.domain.platform.vo.SysMenuVO;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.framework.shiro.util.AuthorizationUtils;
import com.yubb.platform.service.saas.ISaaSMenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 菜单信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:55
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/saasMenu")
public class SaaSMenuController extends BaseController
{
    private String prefix = "platform/saas/menu";

    @Autowired
    private ISaaSMenuService saaSMenuService;

    @RequiresPermissions("platform:saasMenu:view")
    @GetMapping()
    public String menu()
    {
        return prefix + "/menu";
    }

    @RequiresPermissions("platform:saasMenu:list")
    @PostMapping("/list")
    @ResponseBody
    public List<SysMenuVO> list(SysMenuDTO menu)
    {
        List<SysMenuVO> menuList = saaSMenuService.selectMenuList(menu);
        return menuList;
    }

    /**
     * 删除菜单
     */
    @PlatformLog(title = "租户菜单管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("platform:saasMenu:remove")
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id)
    {
        if (saaSMenuService.selectCountMenuByParentId(id) > 0)
        {
            return AjaxResult.warn("存在子菜单,不允许删除");
        }
        if (saaSMenuService.selectCountRoleMenuByMenuId(id) > 0)
        {
            return AjaxResult.warn("菜单已分配,不允许删除");
        }
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(saaSMenuService.deleteMenuById(id));
    }

    /**
     * 新增
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") String parentId, ModelMap mmap)
    {
        SysMenuVO menu = null;
        if (!Constants.TOP_PARENT_MENU_ID.equals(parentId))
        {
            menu = saaSMenuService.selectMenuById(parentId);
        }
        else
        {
            menu = new SysMenuVO();
            menu.setId(Constants.TOP_PARENT_MENU_ID);
            menu.setMenuName("主目录");
        }
        mmap.put("menu", menu);
        return prefix + "/add";
    }

    /**
     * 新增保存菜单
     */
    @PlatformLog(title = "租户菜单管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("platform:saasMenu:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysMenuDTO menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(saaSMenuService.checkMenuNameUnique(menu)))
        {
            return error("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menu.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(saaSMenuService.insertMenu(menu));
    }

    /**
     * 修改菜单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("menu", saaSMenuService.selectMenuById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存菜单
     */
    @PlatformLog(title = "租户菜单管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:saasMenu:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysMenuDTO menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(saaSMenuService.checkMenuNameUnique(menu)))
        {
            return error("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menu.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(saaSMenuService.updateMenu(menu));
    }

    /**
     * 选择菜单图标
     */
    @GetMapping("/icon")
    public String icon()
    {
        return prefix + "/icon";
    }

    /**
     * 校验菜单名称
     */
    @PostMapping("/checkMenuNameUnique")
    @ResponseBody
    public String checkMenuNameUnique(SysMenuDTO menu)
    {
        return saaSMenuService.checkMenuNameUnique(menu);
    }

    /**
     * 加载所有菜单列表树
     */
    @GetMapping("/menuTreeData")
    @ResponseBody
    public List<Ztree> menuTreeData()
    {
        List<Ztree> ztrees = saaSMenuService.menuTreeData();
        return ztrees;
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/selectMenuTree/{id}")
    public String selectMenuTree(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("menu", saaSMenuService.selectMenuById(id));
        return prefix + "/tree";
    }

    /**
     * 加载角色菜单列表树
     */
    @GetMapping("/roleMenuTreeData")
    @ResponseBody
    public List<Ztree> roleMenuTreeData(SysRoleDTO role)
    {
        List<Ztree> ztrees = saaSMenuService.roleMenuTreeData(role, true);
        return ztrees;
    }

}