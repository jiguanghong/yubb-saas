package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.platform.domain.dto.PlatformNoticeDTO;
import com.yubb.platform.domain.vo.PlatformNoticeVO;
import com.yubb.platform.service.IPlatformNoticeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 公告 信息操作处理
 *@Author zhushuyong
 *@Date 2021/5/26 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/notice")
public class PlatformNoticeController extends BaseController
{
    private String prefix = "platform/notice";

    @Autowired
    private IPlatformNoticeService noticeService;

    @RequiresPermissions("platform:notice:view")
    @GetMapping()
    public String notice()
    {
        return prefix + "/notice";
    }

    /**
     * 查询公告列表
     */
    @RequiresPermissions("platform:notice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo<PlatformNoticeVO> list(PlatformNoticeDTO notice)
    {
        return noticeService.selectNoticeList(notice);
    }

    /**
     * 新增公告
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存公告
     */
    @RequiresPermissions("platform:notice:add")
    @PlatformLog(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PlatformNoticeDTO notice)
    {
        notice.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(noticeService.insertNotice(notice));
    }

    /**
     * 修改公告
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("notice", noticeService.selectNoticeById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存公告
     */
    @RequiresPermissions("platform:notice:edit")
    @PlatformLog(title = "通知公告", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlatformNoticeDTO notice)
    {
        notice.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(noticeService.updateNotice(notice));
    }

    /**
     * 删除公告
     */
    @RequiresPermissions("platform:notice:remove")
    @PlatformLog(title = "通知公告", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(noticeService.deleteNoticeByIds(ids));
    }
}
