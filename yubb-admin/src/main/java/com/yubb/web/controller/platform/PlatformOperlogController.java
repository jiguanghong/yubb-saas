package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.platform.domain.dto.PlatformOperLogDTO;
import com.yubb.platform.domain.vo.PlatformOperLogVO;
import com.yubb.platform.service.IPlatformOperLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 操作日志记录
 *@Author zhushuyong
 *@Date 2021/5/26 14:35
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/monitor/operlog")
public class PlatformOperlogController extends BaseController
{
    private String prefix = "platform/monitor/operlog";

    @Autowired
    private IPlatformOperLogService operLogService;

    @RequiresPermissions("platform:monitor:operlog:view")
    @GetMapping()
    public String operlog()
    {
        return prefix + "/operlog";
    }

    @RequiresPermissions("platform:monitor:operlog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlatformOperLogDTO operLog)
    {
        startPage();
        List<PlatformOperLogVO> list = operLogService.selectOperLogList(operLog);
        return getDataTable(list);
    }

    @PlatformLog(title = "操作日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:monitor:operlog:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformOperLogDTO operLog)
    {
        List<PlatformOperLogVO> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<PlatformOperLogVO> util = new ExcelUtil<PlatformOperLogVO>(PlatformOperLogVO.class);
        return util.exportExcel(list, "操作日志");
    }

    @RequiresPermissions("platform:monitor:operlog:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(operLogService.deleteOperLogByIds(ids));
    }

    @RequiresPermissions("platform:monitor:operlog:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("operLog", operLogService.selectOperLogById(id));
        return prefix + "/detail";
    }
    
    @PlatformLog(title = "操作日志", businessType = BusinessType.CLEAN)
    @RequiresPermissions("platform:monitor:operlog:remove")
    @PostMapping("/clean")
    @ResponseBody
    public AjaxResult clean()
    {
        operLogService.cleanOperLog();
        return success();
    }
}
