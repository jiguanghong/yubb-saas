package com.yubb.web.controller.system;

import com.yubb.common.annotation.Log;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.dto.SysUserDTO;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;
import com.yubb.common.core.domain.saas.vo.SysUserVO;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.framework.shiro.util.AuthorizationUtils;
import com.yubb.platform.service.saas.ISaaSRoleService;
import com.yubb.system.domain.SysUserRole;
import com.yubb.system.service.ISysRoleService;
import com.yubb.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 租户查看平台角色信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:44
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/system/pf/role")
public class PfRoleController extends BaseController
{
    private String prefix = "system/role/pf";

    @Autowired
    private ISaaSRoleService saaSRoleService;

    @RequiresPermissions("system:pf:role:view")
    @GetMapping()
    public String role()
    {
        return prefix + "/role";
    }

    @RequiresPermissions("system:pf:role:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysRoleDTO role) {
        startPage();
        List<SysRoleVO> list = saaSRoleService.selectPfRoleList(role);
        return getDataTable(list);
    }

    @GetMapping("/viewRole/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("role", saaSRoleService.selectRoleById(id));
        return prefix + "/viewRole";
    }

}