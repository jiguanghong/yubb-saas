package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.platform.dto.SysDictTypeDTO;
import com.yubb.common.core.domain.platform.vo.SysDictTypeVO;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.platform.service.ISysDictTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 数据字典信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/dict")
public class SysDictTypeController extends BaseController
{
    private String prefix = "platform/dict/type";

    @Autowired
    private ISysDictTypeService dictTypeService;

    @RequiresPermissions("platform:dict:view")
    @GetMapping()
    public String dictType()
    {
        return prefix + "/type";
    }

    @PostMapping("/list")
    @RequiresPermissions("platform:dict:list")
    @ResponseBody
    public TableDataInfo list(SysDictTypeDTO dictType)
    {
        startPage();
        List<SysDictTypeVO> list = dictTypeService.selectDictTypeList(dictType);
        return getDataTable(list);
    }

    @PlatformLog(title = "字典类型", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:dict:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysDictTypeDTO dictType)
    {

        List<SysDictTypeVO> list = dictTypeService.selectDictTypeList(dictType);
        ExcelUtil<SysDictTypeVO> util = new ExcelUtil<SysDictTypeVO>(SysDictTypeVO.class);
        return util.exportExcel(list, "字典类型");
    }

    /**
     * 新增字典类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存字典类型
     */
    @PlatformLog(title = "字典类型", businessType = BusinessType.INSERT)
    @RequiresPermissions("platform:dict:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysDictTypeDTO dict)
    {
        if (UserConstants.DICT_TYPE_NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict)))
        {
            return error("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        dict.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(dictTypeService.insertDictType(dict));
    }

    /**
     * 修改字典类型
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("dict", dictTypeService.selectDictTypeById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存字典类型
     */
    @PlatformLog(title = "字典类型", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:dict:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysDictTypeDTO dict)
    {
        if (UserConstants.DICT_TYPE_NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict)))
        {
            return error("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        dict.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(dictTypeService.updateDictType(dict));
    }

    @PlatformLog(title = "字典类型", businessType = BusinessType.DELETE)
    @RequiresPermissions("platform:dict:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(dictTypeService.deleteDictTypeByIds(ids));
    }

    /**
     * 清空缓存
     */
    @RequiresPermissions("platform:dict:remove")
    @PlatformLog(title = "字典类型", businessType = BusinessType.CLEAN)
    @GetMapping("/clearCache")
    @ResponseBody
    public AjaxResult clearCache()
    {
        dictTypeService.clearCache();
        return success();
    }

    /**
     * 查询字典详细
     */
    @RequiresPermissions("platform:dict:list")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("dict", dictTypeService.selectDictTypeById(id));
        mmap.put("dictList", dictTypeService.selectDictTypeAll());
        return "platform/dict/data/data";
    }

    /**
     * 校验字典类型
     */
    @PostMapping("/checkDictTypeUnique")
    @ResponseBody
    public String checkDictTypeUnique(SysDictTypeDTO dictType)
    {
        return dictTypeService.checkDictTypeUnique(dictType);
    }

    /**
     * 选择字典树
     */
    @GetMapping("/selectDictTree/{columnId}/{dictType}")
    public String selectDeptTree(@PathVariable("columnId") String columnId, @PathVariable("dictType") String dictType,
            ModelMap mmap)
    {
        mmap.put("columnId", columnId);
        mmap.put("dict", dictTypeService.selectDictTypeByType(dictType));
        return prefix + "/tree";
    }

    /**
     * 加载字典列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = dictTypeService.selectDictTree(new SysDictTypeDTO());
        return ztrees;
    }
}
