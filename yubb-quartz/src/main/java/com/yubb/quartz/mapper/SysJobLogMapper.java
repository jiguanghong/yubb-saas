package com.yubb.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.quartz.domain.SysJobLog;
import com.yubb.quartz.domain.dto.SysJobLogDTO;
import com.yubb.quartz.domain.vo.SysJobLogVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *@Description 调度任务日志信息 数据层
 *@Author zhushuyong
 *@Date 2021/6/23 23:20
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysJobLogMapper extends BaseMapper<SysJobLog>
{
    /**
     * 获取quartz调度器日志的计划任务
     * 
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    public List<SysJobLogVO> selectJobLogList(SysJobLogDTO jobLog);

    /**
     * 清空任务日志
     */
    public void cleanJobLog();
}
