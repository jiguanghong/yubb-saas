package com.yubb.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.quartz.domain.SysJob;
import com.yubb.quartz.domain.dto.SysJobDTO;
import com.yubb.quartz.domain.vo.SysJobVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *@Description 调度任务信息 数据层
 *@Author zhushuyong
 *@Date 2021/6/23 23:03
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysJobMapper extends BaseMapper<SysJob>
{
    /**
     * 查询调度任务日志集合
     * 
     * @param job 调度信息
     * @return 操作日志集合
     */
    public List<SysJobVO> selectJobList(SysJobDTO job);

}
