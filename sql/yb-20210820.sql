/*
 Navicat Premium Data Transfer

 Source Server         : 本机MySQL
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : yb

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 20/08/2021 22:53:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- ----------------------------
-- Table structure for platform_dept
-- ----------------------------
DROP TABLE IF EXISTS `platform_dept`;
CREATE TABLE `platform_dept`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门id',
  `parent_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父部门id',
  `ancestors` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `dept_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门类型',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_dept
-- ----------------------------
INSERT INTO `platform_dept` VALUES ('89678424698023676', '0', '0', '圣钰平台科技', 'yb', 0, '圣钰', '15888888888', 'yb@qq.com', '0', '0', 'admin', '2021-05-26 15:36:20', '', NULL);

-- ----------------------------
-- Table structure for platform_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `platform_logininfor`;
CREATE TABLE `platform_logininfor`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for platform_menu
-- ----------------------------
DROP TABLE IF EXISTS `platform_menu`;
CREATE TABLE `platform_menu`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_menu
-- ----------------------------
INSERT INTO `platform_menu` VALUES ('1', '系统管理', '0', 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-05-26 15:36:20', '', NULL, '系统管理目录');
INSERT INTO `platform_menu` VALUES ('100', '用户管理', '1', 1, '/platform/user', '', 'C', '0', '1', 'platform:user:view', 'fa fa-user-o', 'admin', '2021-05-26 15:36:20', '', NULL, '用户管理菜单');
INSERT INTO `platform_menu` VALUES ('1000', '用户查询', '100', 1, '#', '', 'F', '0', '1', 'platform:user:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1001', '用户新增', '100', 2, '#', '', 'F', '0', '1', 'platform:user:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1002', '用户修改', '100', 3, '#', '', 'F', '0', '1', 'platform:user:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1003', '用户删除', '100', 4, '#', '', 'F', '0', '1', 'platform:user:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1004', '用户导出', '100', 5, '#', '', 'F', '0', '1', 'platform:user:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1005', '用户导入', '100', 6, '#', '', 'F', '0', '1', 'platform:user:import', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1006', '重置密码', '100', 7, '#', '', 'F', '0', '1', 'platform:user:resetPwd', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1007', '角色查询', '101', 1, '#', '', 'F', '0', '1', 'platform:role:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1008', '角色新增', '101', 2, '#', '', 'F', '0', '1', 'platform:role:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1009', '角色修改', '101', 3, '#', '', 'F', '0', '1', 'platform:role:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('101', '角色管理', '1', 2, '/platform/role', '', 'C', '0', '1', 'platform:role:view', 'fa fa-user-secret', 'admin', '2021-05-26 15:36:20', '', NULL, '角色管理菜单');
INSERT INTO `platform_menu` VALUES ('1010', '角色删除', '101', 4, '#', '', 'F', '0', '1', 'platform:role:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1011', '角色导出', '101', 5, '#', '', 'F', '0', '1', 'platform:role:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1012', '菜单查询', '102', 1, '#', '', 'F', '0', '1', 'platform:saasMenu:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1013', '菜单新增', '102', 2, '#', '', 'F', '0', '1', 'platform:saasMenu:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1014', '菜单修改', '102', 3, '#', '', 'F', '0', '1', 'platform:saasMenu:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1015', '菜单删除', '102', 4, '#', '', 'F', '0', '1', 'platform:saasMenu:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1016', '部门查询', '103', 1, '#', '', 'F', '0', '1', 'platform:dept:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1017', '部门新增', '103', 2, '#', '', 'F', '0', '1', 'platform:dept:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1018', '部门修改', '103', 3, '#', '', 'F', '0', '1', 'platform:dept:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1019', '部门删除', '103', 4, '#', '', 'F', '0', '1', 'platform:dept:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('102', '租户菜单', '1422767818551267329', 3, '/platform/saasMenu', 'menuItem', 'C', '0', '1', 'platform:saasMenu:view', 'fa fa-th-list', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-04 11:55:11', '菜单管理菜单');
INSERT INTO `platform_menu` VALUES ('1020', '岗位查询', '104', 1, '#', '', 'F', '0', '1', 'platform:post:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1021', '岗位新增', '104', 2, '#', '', 'F', '0', '1', 'platform:post:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1022', '岗位修改', '104', 3, '#', '', 'F', '0', '1', 'platform:post:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1023', '岗位删除', '104', 4, '#', '', 'F', '0', '1', 'platform:post:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1024', '岗位导出', '104', 5, '#', '', 'F', '0', '1', 'platform:post:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1025', '字典查询', '105', 1, '#', '', 'F', '0', '1', 'platform:dict:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1026', '字典新增', '105', 2, '#', '', 'F', '0', '1', 'platform:dict:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1027', '字典修改', '105', 3, '#', '', 'F', '0', '1', 'platform:dict:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1028', '字典删除', '105', 4, '#', '', 'F', '0', '1', 'platform:dict:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1029', '字典导出', '105', 5, '#', '', 'F', '0', '1', 'platform:dict:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('103', '部门管理', '1', 4, '/platform/dept', '', 'C', '0', '1', 'platform:dept:view', 'fa fa-outdent', 'admin', '2021-05-26 15:36:20', '', NULL, '部门管理菜单');
INSERT INTO `platform_menu` VALUES ('1030', '参数查询', '106', 1, '#', '', 'F', '0', '1', 'platform:config:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1031', '参数新增', '106', 2, '#', '', 'F', '0', '1', 'platform:config:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1032', '参数修改', '106', 3, '#', '', 'F', '0', '1', 'platform:config:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1033', '参数删除', '106', 4, '#', '', 'F', '0', '1', 'platform:config:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1034', '参数导出', '106', 5, '#', '', 'F', '0', '1', 'platform:config:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1035', '公告查询', '107', 1, '#', '', 'F', '0', '1', 'platform:notice:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1036', '公告新增', '107', 2, '#', '', 'F', '0', '1', 'platform:notice:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1037', '公告修改', '107', 3, '#', '', 'F', '0', '1', 'platform:notice:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1038', '公告删除', '107', 4, '#', '', 'F', '0', '1', 'platform:notice:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1039', '操作查询', '500', 1, '#', '', 'F', '0', '1', 'platform:monitor:operlog:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('104', '岗位管理', '1', 5, '/platform/post', '', 'C', '0', '1', 'platform:post:view', 'fa fa-address-card-o', 'admin', '2021-05-26 15:36:20', '', NULL, '岗位管理菜单');
INSERT INTO `platform_menu` VALUES ('1040', '操作删除', '500', 2, '#', '', 'F', '0', '1', 'platform:monitor:operlog:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1041', '详细信息', '500', 3, '#', '', 'F', '0', '1', 'platform:monitor:operlog:detail', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1042', '日志导出', '500', 4, '#', '', 'F', '0', '1', 'platform:monitor:operlog:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1043', '登录查询', '501', 1, '#', '', 'F', '0', '1', 'platform:monitor:logininfor:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1044', '登录删除', '501', 2, '#', '', 'F', '0', '1', 'platform:monitor:logininfor:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1045', '日志导出', '501', 3, '#', '', 'F', '0', '1', 'platform:monitor:logininfor:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1046', '账户解锁', '501', 4, '#', '', 'F', '0', '1', 'platform:monitor:logininfor:unlock', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1047', '在线查询', '109', 1, '#', '', 'F', '0', '1', 'platform:monitor:online:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1048', '批量强退', '109', 2, '#', '', 'F', '0', '1', 'platform:monitor:online:batchForceLogout', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1049', '单条强退', '109', 3, '#', '', 'F', '0', '1', 'platform:monitor:online:forceLogout', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('105', '字典管理', '1', 6, '/platform/dict', '', 'C', '0', '1', 'platform:dict:view', 'fa fa-bookmark-o', 'admin', '2021-05-26 15:36:20', '', NULL, '字典管理菜单');
INSERT INTO `platform_menu` VALUES ('1050', '任务查询', '110', 1, '#', '', 'F', '0', '1', 'platform:monitor:job:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1051', '任务新增', '110', 2, '#', '', 'F', '0', '1', 'platform:monitor:job:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1052', '任务修改', '110', 3, '#', '', 'F', '0', '1', 'platform:monitor:job:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1053', '任务删除', '110', 4, '#', '', 'F', '0', '1', 'platform:monitor:job:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1054', '状态修改', '110', 5, '#', '', 'F', '0', '1', 'platform:monitor:job:changeStatus', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1055', '任务详细', '110', 6, '#', '', 'F', '0', '1', 'platform:monitor:job:detail', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1056', '任务导出', '110', 7, '#', '', 'F', '0', '1', 'platform:monitor:job:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1057', '生成查询', '115', 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1058', '生成修改', '115', 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1059', '生成删除', '115', 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('106', '参数设置', '1', 7, '/platform/config', '', 'C', '0', '1', 'platform:config:view', 'fa fa-sun-o', 'admin', '2021-05-26 15:36:20', '', NULL, '参数设置菜单');
INSERT INTO `platform_menu` VALUES ('1060', '预览代码', '115', 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('1061', '生成代码', '115', 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('107', '通知公告', '1', 8, '/platform/notice', '', 'C', '0', '1', 'platform:notice:view', 'fa fa-bullhorn', 'admin', '2021-05-26 15:36:20', '', NULL, '通知公告菜单');
INSERT INTO `platform_menu` VALUES ('108', '日志管理', '1', 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-05-26 15:36:20', '', NULL, '日志管理菜单');
INSERT INTO `platform_menu` VALUES ('109', '在线用户', '2', 1, '/platform/monitor/online', '', 'C', '0', '1', 'platform:monitor:online:view', 'fa fa-user-circle', 'admin', '2021-05-26 15:36:20', '', NULL, '在线用户菜单');
INSERT INTO `platform_menu` VALUES ('110', '定时任务', '2', 2, '/platform/monitor/job', '', 'C', '0', '1', 'platform:monitor:job:view', 'fa fa-tasks', 'admin', '2021-05-26 15:36:20', '', NULL, '定时任务菜单');
INSERT INTO `platform_menu` VALUES ('111', '数据监控', '2', 3, '/platform/monitor/data', '', 'C', '0', '1', 'platform:monitor:data:view', 'fa fa-bug', 'admin', '2021-05-26 15:36:20', '', NULL, '数据监控菜单');
INSERT INTO `platform_menu` VALUES ('112', '服务监控', '2', 4, '/platform/monitor/server', '', 'C', '0', '1', 'platform:monitor:server:view', 'fa fa-server', 'admin', '2021-05-26 15:36:20', '', NULL, '服务监控菜单');
INSERT INTO `platform_menu` VALUES ('113', '缓存监控', '2', 5, '/platform/monitor/cache', '', 'C', '0', '1', 'platform:monitor:cache:view', 'fa fa-cube', 'admin', '2021-05-26 15:36:20', '', NULL, '缓存监控菜单');
INSERT INTO `platform_menu` VALUES ('114', '表单构建', '3', 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2021-05-26 15:36:20', '', NULL, '表单构建菜单');
INSERT INTO `platform_menu` VALUES ('115', '代码生成', '3', 2, '/tool/gen1', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2021-05-26 15:36:20', '', NULL, '代码生成菜单');
INSERT INTO `platform_menu` VALUES ('116', '系统接口', '3', 3, '/tool/swagger', 'menuItem', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-16 14:10:44', '系统接口菜单');
INSERT INTO `platform_menu` VALUES ('1422767818551267329', '租户管理', '0', 2, '', 'menuItem', 'M', '0', '1', '', 'fa fa-bank', 'admin', '2021-08-04 11:54:16', 'admin', '2021-08-04 11:54:16', '');
INSERT INTO `platform_menu` VALUES ('1423511840943562753', '租户角色', '1422767818551267329', 2, '/platform/saasRole', 'menuItem', 'C', '0', '1', 'platform:saasRole:view', 'fa fa-user-secret', 'admin', '2021-08-06 13:10:45', 'admin', '2021-08-06 13:10:45', '');
INSERT INTO `platform_menu` VALUES ('1423512072255234049', '查询', '1423511840943562753', 1, '', 'menuItem', 'F', '0', '1', 'platform:saasRole:list', '', 'admin', '2021-08-06 13:11:40', 'admin', '2021-08-06 13:11:40', '');
INSERT INTO `platform_menu` VALUES ('1423512193432870914', '新增', '1423511840943562753', 2, '', 'menuItem', 'F', '0', '1', 'platform:saasRole:add', '', 'admin', '2021-08-06 13:12:09', 'admin', '2021-08-06 13:12:09', '');
INSERT INTO `platform_menu` VALUES ('1423512294935027714', '修改', '1423511840943562753', 3, '', 'menuItem', 'F', '0', '1', 'platform:saasRole:edit', '', 'admin', '2021-08-06 13:12:33', 'admin', '2021-08-06 13:12:33', '');
INSERT INTO `platform_menu` VALUES ('1423512466557558785', '删除', '1423511840943562753', 4, '', 'menuItem', 'F', '0', '1', 'platform:saasRole:remove', '', 'admin', '2021-08-06 13:13:14', 'admin', '2021-08-06 13:13:14', '');
INSERT INTO `platform_menu` VALUES ('1423512555762016257', '导出', '1423511840943562753', 5, '', 'menuItem', 'F', '0', '1', 'platform:saasRole:export', '', 'admin', '2021-08-06 13:13:36', 'admin', '2021-08-06 13:13:36', '');
INSERT INTO `platform_menu` VALUES ('2', '系统监控', '0', 3, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-04 11:54:29', '系统监控目录');
INSERT INTO `platform_menu` VALUES ('2346345634563456', '租户列表', '1422767818551267329', 1, '/platform/tenant', 'menuItem', 'C', '0', '1', 'platform:tenant:view', '#', 'admin', '2021-07-03 17:35:54', 'admin', '2021-08-04 17:28:52', '租户列表菜单');
INSERT INTO `platform_menu` VALUES ('3', '系统工具', '0', 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2021-05-26 15:36:20', '', NULL, '系统工具目录');
INSERT INTO `platform_menu` VALUES ('4', '圣钰官网', '0', 4, 'https://gitee.com/jinzheyi/yubb-saas', 'menuBlank', 'C', '0', '1', '', 'fa fa-location-arrow', 'admin', '2021-05-26 15:36:20', '', NULL, '圣钰官网地址');
INSERT INTO `platform_menu` VALUES ('500', '操作日志', '108', 1, '/platform/monitor/operlog', '', 'C', '0', '1', 'platform:monitor:operlog:view', 'fa fa-address-book', 'admin', '2021-05-26 15:36:20', '', NULL, '操作日志菜单');
INSERT INTO `platform_menu` VALUES ('501', '登录日志', '108', 2, '/platform/monitor/logininfor', '', 'C', '0', '1', 'platform:monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2021-05-26 15:36:20', '', NULL, '登录日志菜单');
INSERT INTO `platform_menu` VALUES ('5674567456745673', '租户查询', '2346345634563456', 1, '#', '', 'F', '0', '1', 'platform:tenant:list', '#', 'admin', '2021-07-03 17:35:54', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('5675674534743564', '租户新增', '2346345634563456', 2, '#', '', 'F', '0', '1', 'platform:tenant:add', '#', 'admin', '2021-07-03 17:35:54', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('634564634567', '菜单管理', '1', 3, '/platform/menu', '', 'C', '0', '1', 'platform:saasMenu:view', 'fa fa-th-list', 'admin', '2021-05-26 15:36:20', '', NULL, '菜单管理菜单');
INSERT INTO `platform_menu` VALUES ('63456463456701', '菜单查询', '634564634567', 1, '#', '', 'F', '0', '1', 'platform:menu:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('63456463456702', '菜单新增', '634564634567', 2, '#', '', 'F', '0', '1', 'platform:menu:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('63456463456703', '菜单修改', '634564634567', 3, '#', '', 'F', '0', '1', 'platform:menu:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('63456463456704', '菜单删除', '634564634567', 4, '#', '', 'F', '0', '1', 'platform:menu:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('7890678967976856', '租户修改', '2346345634563456', 3, '#', '', 'F', '0', '1', 'platform:tenant:edit', '#', 'admin', '2021-07-03 17:35:54', '', NULL, '');
INSERT INTO `platform_menu` VALUES ('7967856734356235', '租户导出', '2346345634563456', 5, '#', '', 'F', '0', '1', 'platform:tenant:export', '#', 'admin', '2021-07-03 17:35:54', '', NULL, '');

-- ----------------------------
-- Table structure for platform_notice
-- ----------------------------
DROP TABLE IF EXISTS `platform_notice`;
CREATE TABLE `platform_notice`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_notice
-- ----------------------------

-- ----------------------------
-- Table structure for platform_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `platform_oper_log`;
CREATE TABLE `platform_oper_log`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for platform_post
-- ----------------------------
DROP TABLE IF EXISTS `platform_post`;
CREATE TABLE `platform_post`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_post
-- ----------------------------

-- ----------------------------
-- Table structure for platform_role
-- ----------------------------
DROP TABLE IF EXISTS `platform_role`;
CREATE TABLE `platform_role`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_role
-- ----------------------------
INSERT INTO `platform_role` VALUES ('674567345645676783889', '超级管理员', 'platform_admin', 1, '1', '0', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '超级管理员');

-- ----------------------------
-- Table structure for platform_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `platform_role_dept`;
CREATE TABLE `platform_role_dept`  (
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `dept_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for platform_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `platform_role_menu`;
CREATE TABLE `platform_role_menu`  (
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `menu_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for platform_tenant
-- ----------------------------
DROP TABLE IF EXISTS `platform_tenant`;
CREATE TABLE `platform_tenant`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `tenant_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '租户名称',
  `tenant_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '租户编号',
  `logo_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户logo',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '租户状态（0正常 1停用）',
  `remark` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `start_date` datetime(0) NULL DEFAULT NULL COMMENT '租户系统开始时间',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '租户系统到期时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '租户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_tenant
-- ----------------------------
INSERT INTO `platform_tenant` VALUES ('1411258341581467649', '一号租户', '0000000001', '', '0', '', '2021-08-04 00:00:00', '2050-03-07 00:00:00', 'admin', '2021-07-03 17:39:43', 'admin', '2021-08-12 20:25:33');

-- ----------------------------
-- Table structure for platform_user
-- ----------------------------
DROP TABLE IF EXISTS `platform_user`;
CREATE TABLE `platform_user`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `dept_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime(0) NULL DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_user
-- ----------------------------
INSERT INTO `platform_user` VALUES ('54634575686785679498', '89678424698023676', 'admin', '圣钰-平台管理员', 'yb', 'yubb@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2021-08-20 22:49:06', '2021-05-26 15:36:20', 'admin', '2021-05-26 15:36:20', 'yubb-system', '2021-08-20 22:49:06', '管理员');

-- ----------------------------
-- Table structure for platform_user_online
-- ----------------------------
DROP TABLE IF EXISTS `platform_user_online`;
CREATE TABLE `platform_user_online`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for platform_user_post
-- ----------------------------
DROP TABLE IF EXISTS `platform_user_post`;
CREATE TABLE `platform_user_post`  (
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `post_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for platform_user_role
-- ----------------------------
DROP TABLE IF EXISTS `platform_user_role`;
CREATE TABLE `platform_user_role`  (
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_user_role
-- ----------------------------
INSERT INTO `platform_user_role` VALUES ('54634575686785679498', '674567345645676783889');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES ('4', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('5', '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES ('6', '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES ('7', '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES ('8', '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES ('9', '主框架页-是否开启页脚', 'sys.index.ignoreFooter', 'true', 'Y', 'admin', '2021-05-26 15:36:21', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门id',
  `parent_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父部门id',
  `ancestors` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `dept_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门类型',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('567456834562247092', '0', '0', '圣钰SaaS科技', 'yb', 0, 'admin', '15888888888', 'yubb@qq.com', '0', '0', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-20 22:06:29', '1411258341581467649');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES ('10', 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES ('15', 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES ('16', 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES ('19', 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES ('2', 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES ('20', 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES ('21', 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES ('22', 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES ('23', 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES ('24', 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES ('25', 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES ('26', 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES ('27', 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES ('28', 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('29', 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('3', 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-05-26 15:36:21', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('1422742173800628226', '完成状态', 'accomplish_status', '0', 'admin', '2021-08-04 10:12:22', 'admin', '2021-08-04 10:12:22', '');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2021-05-26 15:36:20', '', NULL, '操作类型列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ybTask.ybNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 15:36:21', '', NULL, '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ybTask.ybParams(\'yb\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 15:36:21', '', NULL, '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ybTask.ybMultipleParams(\'yb\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 15:36:21', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_c_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '菜单使用类型\r\n00   基本菜单（租户拥有的最基础的菜单，租户永久拥有）\r\n01   免费赠送菜单（以订单收费为0的形式赠送，收回的话直接修改订单）\r\n02   付费菜单（租户购买之后，有效期之内可以任意使用）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', 1, '#', '', '00', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-05-26 15:36:20', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', 1, '/system/user', '', '00', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2021-05-26 15:36:20', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', 1, '#', '', '00', 'F', '0', '1', 'system:user:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', 2, '#', '', '00', 'F', '0', '1', 'system:user:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', 3, '#', '', '00', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', 4, '#', '', '00', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', 5, '#', '', '00', 'F', '0', '1', 'system:user:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导入', '100', 6, '#', '', '00', 'F', '0', '1', 'system:user:import', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1006', '重置密码', '100', 7, '#', '', '00', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1007', '角色查询', '101', 1, '#', '', '00', 'F', '0', '1', 'system:role:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1008', '角色新增', '101', 2, '#', '', '00', 'F', '0', '1', 'system:role:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1009', '角色修改', '101', 3, '#', '', '00', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('101', '内部角色', '1426117002031804417', 1, '/system/role', 'menuItem', '00', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-13 17:46:14', '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('1010', '角色删除', '101', 4, '#', '', '00', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1011', '角色导出', '101', 5, '#', '', '00', 'F', '0', '1', 'system:role:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1016', '部门查询', '103', 1, '#', '', '00', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1017', '部门新增', '103', 2, '#', '', '00', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1018', '部门修改', '103', 3, '#', '', '00', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1019', '部门删除', '103', 4, '#', '', '00', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位查询', '104', 1, '#', '', '00', 'F', '0', '1', 'system:post:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位新增', '104', 2, '#', '', '00', 'F', '0', '1', 'system:post:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位修改', '104', 3, '#', '', '00', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位删除', '104', 4, '#', '', '00', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位导出', '104', 5, '#', '', '00', 'F', '0', '1', 'system:post:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', 4, '/system/dept', '', '00', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2021-05-26 15:36:20', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('1035', '公告查询', '107', 1, '#', '', '00', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1036', '公告新增', '107', 2, '#', '', '00', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1037', '公告修改', '107', 3, '#', '', '00', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1038', '公告删除', '107', 4, '#', '', '00', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1039', '操作查询', '500', 1, '#', '', '00', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', 5, '/system/post', '', '00', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2021-05-26 15:36:20', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('1040', '操作删除', '500', 2, '#', '', '00', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1041', '详细信息', '500', 3, '#', '', '00', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', 4, '#', '', '00', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', 1, '#', '', '00', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', 2, '#', '', '00', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', 3, '#', '', '00', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1046', '账户解锁', '501', 4, '#', '', '00', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1047', '在线查询', '109', 1, '#', '', '00', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1048', '批量强退', '109', 2, '#', '', '00', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('1049', '单条强退', '109', 3, '#', '', '00', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2021-05-26 15:36:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', 8, '/system/notice', '', '00', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2021-05-26 15:36:20', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', 10, '#', '', '00', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-05-26 15:36:20', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '1423806481886625793', 9, '/monitor/online', 'menuItem', '00', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-07 08:42:09', '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('1423806481886625793', '系统监控', '0', 2, '', 'menuItem', '00', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-08-07 08:41:33', 'admin', '2021-08-07 08:41:33', '');
INSERT INTO `sys_menu` VALUES ('1426117002031804417', '角色管理', '1', 2, '', 'menuItem', '00', 'M', '0', '1', '', 'fa fa-street-view', 'admin', '2021-08-13 17:42:44', 'admin', '2021-08-13 17:42:44', '');
INSERT INTO `sys_menu` VALUES ('1426117817731657730', '平台角色', '1426117002031804417', 2, '/system/pf/role', 'menuItem', '00', 'C', '0', '1', 'system:pf:role:view', 'fa fa-users', 'admin', '2021-08-13 17:45:58', 'admin', '2021-08-13 17:45:58', '');
INSERT INTO `sys_menu` VALUES ('1426118022732460033', '查询', '1426117817731657730', 1, '', 'menuItem', '00', 'F', '0', '1', 'system:pf:role:list', '', 'admin', '2021-08-13 17:46:47', 'admin', '2021-08-13 17:46:47', '');
INSERT INTO `sys_menu` VALUES ('4', '圣钰官网', '0', 4, 'https://gitee.com/jinzheyi/yubb-saas', 'menuBlank', '00', 'C', '0', '1', '', 'fa fa-location-arrow', 'admin', '2021-05-26 15:36:20', '', NULL, '圣钰官网地址');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', 1, '/monitor/operlog', '', '00', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2021-05-26 15:36:20', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', 2, '/monitor/logininfor', '', '00', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2021-05-26 15:36:20', '', NULL, '登录日志菜单');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1424613086572085250', '董事长', '4787b203faa14f12b2bbc5611aba2157', 1, '1', '0', '0', 'admin', '2021-08-09 14:06:43', 'admin', '2021-08-09 14:06:43', '', 'platform_saas');
INSERT INTO `sys_role` VALUES ('54634567845232344567897', '租户管理员角色', 'saas_admin', 0, '1', '0', '0', 'admin', '2021-05-26 15:36:20', 'admin', '2021-08-13 17:48:15', '租户管理员角色', 'platform_saas');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `dept_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门ID',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `menu_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '100');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1000');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1001');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1002');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1003');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1004');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1005');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1006');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1016');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1017');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1018');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1019');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1020');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1021');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1022');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1023');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1024');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '103');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1035');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1036');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1037');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1038');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1039');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '104');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1040');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1041');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1042');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1043');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1044');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1045');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1046');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1047');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1048');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1049');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '107');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '108');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '109');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '1423806481886625793');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '4');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '500');
INSERT INTO `sys_role_menu` VALUES ('1424613086572085250', '501');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '100');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1000');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1001');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1002');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1003');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1004');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1005');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1006');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1007');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1008');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1009');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '101');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1010');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1011');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1016');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1017');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1018');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1019');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1020');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1021');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1022');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1023');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1024');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '103');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1035');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1036');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1037');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1038');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1039');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '104');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1040');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1041');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1042');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1043');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1044');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1045');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1046');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1047');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1048');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1049');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '107');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '108');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '109');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1423806481886625793');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1426117002031804417');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1426117817731657730');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '1426118022732460033');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '4');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '500');
INSERT INTO `sys_role_menu` VALUES ('54634567845232344567897', '501');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `dept_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户02租户超管）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime(0) NULL DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1408064610237779960', '567456834562247092', 'admin', '圣钰saas', 'yb', 'yubb@qq.com', '15888888888', '1', '/profile/avatar/2021/08/12/efe9fc9a-9b09-4b79-974c-401e1585c715.png', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2021-08-20 22:35:52', '2021-05-26 15:36:20', 'admin', '2021-05-26 15:36:20', 'yubb-system', '2021-08-20 22:35:52', '管理员', '1411258341581467649');

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `post_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位ID',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1408064610237779960', '54634567845232344567897', '1411258341581467649');

SET FOREIGN_KEY_CHECKS = 1;
